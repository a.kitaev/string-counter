using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace string_counter {
    public class Model {
        private bool _isMultilineComment;

        public int GetLinesCount(FileSystemInfo info) {
            using (var fileStream = new StreamReader(info.FullName, Encoding.UTF8)) {
                var lines = 0;
                string str;
                while ((str = fileStream.ReadLine()) != null) {
                    if (!IsEmpty(str) && !IsMultilineComment(str) && !IsSingleComment(str)) {
                        lines++;
                    }
                }

                return lines;
            }
        }

        private bool IsEmpty(string line) {
            return line.Length == 0;
        }

        private bool IsSingleComment(string line) {
            return line.IndexOf("//", StringComparison.Ordinal) == 0;
        }

        private bool IsMultilineComment(string line) {
            var indexOpen = line.IndexOf("/*", StringComparison.Ordinal);
            var indexClose = line.IndexOf("*/", StringComparison.Ordinal);

            if (indexOpen != -1 && indexClose != -1) {
                _isMultilineComment = false;
                return true;
            }

            if (indexOpen != -1 && indexClose == -1) {
                _isMultilineComment = true;
                return true;
            }

            if (indexOpen == -1 && indexClose != -1) {
                _isMultilineComment = false;
                return false;
            }

            return _isMultilineComment;
        }

        public IEnumerable<FileInfo> FilterFileInfo(IEnumerable<FileInfo> files, IEnumerable<Regex> regexes) {
            return files
                .Where(fileInfo => regexes.Select(regex => regex.IsMatch(fileInfo.Name)).Any(isMatch => isMatch))
                .ToList();
        }

        public IEnumerable<FileInfo> GetAllFiles(string path) {
            var dirs = GetAllDirectories(new DirectoryInfo(path));
            var files = dirs.SelectMany(directoryInfo => directoryInfo.GetFiles());
            return files;
        }

        private IEnumerable<DirectoryInfo> GetAllDirectories(DirectoryInfo currentDir) {
            var list = new List<DirectoryInfo> {currentDir};
            var subDirectories = currentDir.GetDirectories();
            list.AddRange(subDirectories.SelectMany(GetAllDirectories));
            return list;
        }

        public IEnumerable<Regex> GetRegexList(IEnumerable<string> args) {
            return args.Select(arg => "^." + arg.Replace(".", "\\.(") + ")$")
                .Select(fixPattern => new Regex(fixPattern)).ToList();
        }
    }
}