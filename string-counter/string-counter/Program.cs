﻿using System;

namespace string_counter {
    internal static class Program {
        private static void Main(string[] args) {
            var counter = new Model();
            var files = counter.FilterFileInfo(counter.GetAllFiles("."), counter.GetRegexList(args));
            foreach (var file in files) {
                var linesCount = counter.GetLinesCount(file);
                Console.WriteLine("File \"" + file.Name + "\" has " + linesCount + " string");
            }
        }
    }
}